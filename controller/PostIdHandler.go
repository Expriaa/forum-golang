package handlers

import (
	"database/sql"
	fonctions "forum/controller/fonctions"
	structures "forum/models"
	"net/http"
	"text/template"
)

func PostIdHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/show" {
		NotFound(w, r, http.StatusNotFound)
		return
	}
	Id := r.FormValue("id")

	files := []string{"view/html/show.html",
		"view/html/templates/head.html",
		"view/html/templates/footer.html",
		"view/html/templates/header/navbar.html",
		"view/html/templates/header/navbarco.html",
		"view/html/templates/header/navbarlogin.html",
		"view/html/templates/header/navbarsignup.html"}
	t := template.Must(template.ParseFiles(files...))

	switch r.Method {
	case "GET":
		var post structures.Post

		db, _ := sql.Open("sqlite3", "./Database.db")
		res, err := db.Query("SELECT p.title, p.content, u.id, u.pseudo FROM Post p INNER JOIN User u ON p.creator = u.id WHERE p.id = (?)", Id)
		fonctions.CheckErr(err)

		for res.Next() {
			err = res.Scan(&post.Title, &post.Content, &post.CreatorId, &post.Creator)
			fonctions.CheckErr(err)
		}
		if post.Title == "" {
			NotFound(w, r, http.StatusNotFound)
			return
		}

		var comments []structures.Comment
		var comment structures.Comment
		res, err = db.Query("SELECT c.content, u.pseudo FROM Comment c INNER JOIN User u ON c.creator = u.id INNER JOIN Post p ON c.post_id = p.id WHERE p.id = (?)", Id)
		fonctions.CheckErr(err)

		for res.Next() {
			err = res.Scan(&comment.Content, &comment.Creator)
			fonctions.CheckErr(err)
			comments = append(comments, comment)
		}

		uid := structures.InfoPost{Username: fonctions.CookieGetUsername(r), Post: post, Comments: comments}
		t.Execute(w, uid)
	case "POST":
		content := r.Form.Get("content")
		db, _ := sql.Open("sqlite3", "./Database.db")
		stmt, err := db.Prepare("INSERT INTO Comment (content, creator, post_id) VALUES (?, ?, ?)")
		fonctions.CheckErr(err)

		_, err = stmt.Exec(content, fonctions.CookieGetid(r), Id)
		fonctions.CheckErr(err)
		http.Redirect(w, r, "/show?id="+Id, 302)
	}

}
