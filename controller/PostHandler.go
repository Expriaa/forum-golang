package handlers

import (
	"database/sql"
	"fmt"
	fonctions "forum/controller/fonctions"
	structures "forum/models"
	"net/http"
	"text/template"
)

func PostHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/post" {
		NotFound(w, r, http.StatusNotFound)
		return
	}

	switch r.Method {
	case "GET":
		files := []string{"view/html/post.html",
			"view/html/templates/head.html",
			"view/html/templates/footer.html",
			"view/html/templates/header/navbar.html",
			"view/html/templates/header/navbarlogin.html",
			"view/html/templates/header/navbarco.html",
			"view/html/templates/header/navbarsignup.html"}
		t := template.Must(template.ParseFiles(files...))
		_, err := r.Cookie("session")
		if err != nil {
			http.Redirect(w, r, "/login", 302)
		}
		uid := structures.Info{Username: fonctions.CookieGetUsername(r)}
		t.Execute(w, uid)
	case "POST":
		if err := r.ParseForm(); err != nil {
			fmt.Printf("ParseForm() err: %v", err)
			return
		}

		title := r.Form.Get("postname")
		content := r.Form.Get("post")
		db, _ := sql.Open("sqlite3", "./Database.db")
		stmt, err := db.Prepare(`INSERT INTO post (creator, title, content)
		VALUES (?, ?, ?)`)
		_, err2 := stmt.Exec(fonctions.CookieGetid(r), title, content)
		if err != nil {
			fmt.Println(err)
		}
		if err2 != nil {
			fmt.Println(err2)
		}
		http.Redirect(w, r, "/", 301)
	}

}
