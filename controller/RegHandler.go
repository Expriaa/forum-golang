package handlers

import (
	"database/sql"
	"fmt"
	fonctions "forum/controller/fonctions"
	structures "forum/models"
	"net/http"
	"text/template"

	uuid "github.com/satori/go.uuid"
)

func RegHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/register" {
		NotFound(w, r, http.StatusNotFound)
		return
	}

	switch r.Method {
	case "GET":
		files := []string{"view/html/register.html",
			"view/html/templates/head.html",
			"view/html/templates/footer.html",
			"view/html/templates/header/navbar.html",
			"view/html/templates/header/navbarlogin.html",
			"view/html/templates/header/navbarsignup.html"}
		t := template.Must(template.ParseFiles(files...))
		uid := structures.Info{Username: fonctions.CookieGetUsername(r)}
		t.Execute(w, uid)
	case "POST":
		if err := r.ParseForm(); err != nil {
			fmt.Printf("ParseForm() err: %v", err)
			return
		}
		var u2 = uuid.NewV4().String()
		firstname := r.Form.Get("firstname")
		lastname := r.Form.Get("lastname")
		email := r.Form.Get("email")
		pseudo := r.Form.Get("pseudo")
		password := r.Form.Get("password")
		hash := HashPassword(password)
		db, _ := sql.Open("sqlite3", "./Database.db")
		stmt, err := db.Prepare(`INSERT INTO user (UUID, email, password, pseudo, first_name, last_name)	
		VALUES (?, ?, ?, ?, ?, ?)`)
		_, err2 := stmt.Exec(u2, email, hash, pseudo, firstname, lastname)
		if err != nil {
			fmt.Println(err)
		}
		if err2 != nil {
			fmt.Println(err2)
		}
		http.Redirect(w, r, "/", 301)
	}
}
