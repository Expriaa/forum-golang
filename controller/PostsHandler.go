package handlers

import (
	"database/sql"
	"fmt"
	fonctions "forum/controller/fonctions"
	structures "forum/models"
	"log"
	"net/http"
	"text/template"
)

func PostsHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/posts" {
		NotFound(w, r, http.StatusNotFound)
		return
	}
	PostsTab := []structures.Post{}

	db, _ := sql.Open("sqlite3", "./Database.db")
	res, err := db.Query("SELECT p.id, p.title, p.content, p.favorite, p.unfavorite, p.creator, u.pseudo FROM Post p LEFT JOIN User u ON p.creator = u.id")
	if err != nil {
		fmt.Println(err)
	}

	for res.Next() {
		var post structures.Post
		err3 := res.Scan(&post.Id, &post.Title, &post.Content, &post.Like, &post.Dislike, &post.CreatorId, &post.Creator)
		if err3 != nil {
			log.Fatal(err3)
		}
		PostsTab = append(PostsTab, post)
	}
	files := []string{"view/html/posts.html",
		"view/html/templates/head.html",
		"view/html/templates/footer.html",
		"view/html/templates/header/navbar.html",
		"view/html/templates/header/navbarlogin.html",
		"view/html/templates/header/navbarco.html",
		"view/html/templates/header/navbarsignup.html"}
	data := structures.Info{
		Username: fonctions.CookieGetUsername(r),
		PostsTab: PostsTab,
	}
	t := template.Must(template.ParseFiles(files...))

	err4 := t.Execute(w, data)
	if err4 != nil {
		fmt.Println(err4)
	}

}
