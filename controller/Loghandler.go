package handlers

import (
	"database/sql"
	"fmt"
	fonctions "forum/controller/fonctions"
	structures "forum/models"
	"log"
	"net/http"
	"text/template"
	"time"

	"golang.org/x/crypto/bcrypt"
)

func HashPassword(password string) string {
	pw := []byte(password)
	res, _ := bcrypt.GenerateFromPassword(pw, bcrypt.DefaultCost)
	return string(res)
}

func ComparePassword(HashPassword string, password string) error {
	pw := []byte(password)
	hw := []byte(HashPassword)
	err := bcrypt.CompareHashAndPassword(hw, pw)
	fmt.Println(pw, hw)
	fmt.Println(err)
	return err
}

func LogHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/login" {
		NotFound(w, r, http.StatusNotFound)
		return
	}
	switch r.Method {
	case "GET":
		files := []string{"view/html/login.html",
			"view/html/templates/head.html",
			"view/html/templates/footer.html",
			"view/html/templates/header/navbar.html",
			"view/html/templates/header/navbarlogin.html",
			"view/html/templates/header/navbarsignup.html"}
		t := template.Must(template.ParseFiles(files...))
		uid := structures.Info{Username: fonctions.CookieGetUsername(r)}
		t.Execute(w, uid)
	case "POST":
		if err := r.ParseForm(); err != nil {
			fmt.Printf("ParseForm() err: %v", err)
			return

		}
		db, _ := sql.Open("sqlite3", "./Database.db")
		email := r.Form.Get("email")
		password := r.Form.Get("password")
		stmt, err := db.Prepare(`SELECT password, UUID FROM user WHERE email = ?`)
		rows, err2 := stmt.Query(email)
		var connect_password string
		var UUID string
		if err != nil {
			log.Fatal(err)
		}
		defer rows.Close()
		for rows.Next() {
			err3 := rows.Scan(&connect_password, &UUID)
			if err3 != nil {
				log.Fatal(err3)
			}
			fmt.Println(connect_password)
		}
		if err2 != nil {
			fmt.Println(err2)
		}
		if ComparePassword(connect_password, password) != nil {
			fmt.Println("mauvais password")
		} else {
			Cookie := &http.Cookie{
				Name:    "session",
				Value:   UUID,
				Expires: time.Time{},
				MaxAge:  3600,
			}
			http.SetCookie(w, Cookie)
			for _, c := range r.Cookies() {
				fmt.Println(c.Name)
				fmt.Println(c.Value)
			}

			http.Redirect(w, r, "/", 301)
		}
	}
}
