package handlers

import (
	fonctions "forum/controller/fonctions"
	structures "forum/models"
	"net/http"
	"text/template"
)

func NotFound(w http.ResponseWriter, r *http.Request, status int) {
	w.WriteHeader(status)

	files := []string{"view/html/404.html",
		"view/html/templates/head.html",
		"view/html/templates/footer.html",
		"view/html/templates/header/navbar.html",
		"view/html/templates/header/navbarlogin.html",
		"view/html/templates/header/navbarco.html",
		"view/html/templates/header/navbarsignup.html"}
	t := template.Must(template.ParseFiles(files...))

	uid := structures.Info{Username: fonctions.CookieGetUsername(r)}
	t.Execute(w, uid)
}
