package handlers

import (
	"database/sql"
	"fmt"
	fonctions "forum/controller/fonctions"
	structures "forum/models"
	"net/http"
	"text/template"
)

func ProfilsHandler(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/profil" {
		NotFound(w, r, http.StatusNotFound)
		return
	}
	files := []string{"view/html/profil.html",
		"view/html/templates/head.html",
		"view/html/templates/footer.html",
		"view/html/templates/header/navbar.html",
		"view/html/templates/header/navbarlogin.html",
		"view/html/templates/header/navbarco.html",
		"view/html/templates/header/navbarsignup.html"}

	t := template.Must(template.ParseFiles(files...))
	switch r.Method {
	case "GET":
		Id := r.FormValue("id")
		if Id == "" && !fonctions.IsConnected(r) {
			http.Redirect(w, r, "/login", 302)
		} else if Id == "" || Id == fmt.Sprint(fonctions.CookieGetid(r)) {
			user := fonctions.CookieGetAllInfos(r)
			posts := fonctions.CookieGetAllPosts(r)
			var UserPage = structures.UserPage{
				Username: fonctions.CookieGetUsername(r),
				User:     user,
				Posts:    posts,
			}
			t.Execute(w, UserPage)
		} else {
			db, err := sql.Open("sqlite3", "./Database.db")
			fonctions.CheckErr(err)
			row, err := db.Query("SELECT pseudo, email, first_name, last_name FROM user WHERE id = ?", Id)
			fonctions.CheckErr(err)
			var user structures.User
			defer row.Close()
			for row.Next() {
				row.Scan(&user.Username, &user.Email, &user.FirstName, &user.LastName)
			}

			row, err = db.Query("SELECT p.id, p.title FROM post p INNER JOIN user u ON p.creator = u.id WHERE p.creator = ?", Id)
			fonctions.CheckErr(err)
			var posts []structures.Post
			defer row.Close()
			for row.Next() {
				var post structures.Post
				row.Scan(&post.Id, &post.Title)
				posts = append(posts, post)
			}
			var UserPage = structures.UserPage{
				Username: fonctions.CookieGetUsername(r),
				User:     user,
				Posts:    posts,
			}
			t.Execute(w, UserPage)
		}

	case "POST":
	}
}
