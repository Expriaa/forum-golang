package handlers

import (
	"fmt"
	"net/http"
	"time"
)

func LogoutHandler(response http.ResponseWriter, request *http.Request) {
	cookie := &http.Cookie{
		Name:    "session",
		Value:   "",
		Path:    "/",
		Expires: time.Unix(0, 0),
	}
	http.SetCookie(response, cookie)
	fmt.Println("Session cleared")
	http.Redirect(response, request, "/", http.StatusSeeOther)

}
