package handlers

import (
	fonctions "forum/controller/fonctions"
	structures "forum/models"
	"net/http"
	"text/template"
)

func UserHandler(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/user" {
		NotFound(w, r, http.StatusNotFound)
		return
	}

	files := []string{"view/html/user.html",
		"view/html/templates/head.html",
		"view/html/templates/footer.html",
		"view/html/templates/header/navbar.html",
		"view/html/templates/header/navbarlogin.html",
		"view/html/templates/header/navbarsignup.html"}
	t := template.Must(template.ParseFiles(files...))

	uid := structures.Info{Username: fonctions.CookieGetUsername(r)}
	t.Execute(w, uid)
}
