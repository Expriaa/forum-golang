package fonctions

import (
	"database/sql"
	structures "forum/models"
	"net/http"
)

func CookieGetUsername(r *http.Request) string {
	db, err := sql.Open("sqlite3", "./Database.db")
	CheckErr(err)
	cookie, err := r.Cookie("session")
	if err != nil {
		return ""
	}
	row, err := db.Query("SELECT pseudo FROM user WHERE uuid = ?", cookie.Value)
	CheckErr(err)
	var username string
	defer row.Close()
	for row.Next() {
		row.Scan(&username)
	}
	return (username)
}

func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

func CookieGetid(r *http.Request) int {
	db, err := sql.Open("sqlite3", "./Database.db")
	CheckErr(err)
	cookie, err := r.Cookie("session")
	if err != nil {
		return 0
	}
	row, err := db.Query("SELECT id FROM user WHERE uuid = ?", cookie.Value)
	CheckErr(err)
	var id int
	defer row.Close()
	for row.Next() {
		row.Scan(&id)
	}
	return (id)
}

func CookieGetAllInfos(r *http.Request) structures.User {
	db, err := sql.Open("sqlite3", "./Database.db")
	CheckErr(err)
	cookie, err := r.Cookie("session")
	if err != nil {
		return structures.User{}
	}
	row, err := db.Query("SELECT pseudo, email, first_name, last_name FROM user WHERE uuid = ?", cookie.Value)
	CheckErr(err)
	var user structures.User
	defer row.Close()
	for row.Next() {
		row.Scan(&user.Username, &user.Email, &user.FirstName, &user.LastName)
	}
	return user
}

func CookieGetAllPosts(r *http.Request) []structures.Post {
	db, err := sql.Open("sqlite3", "./Database.db")
	CheckErr(err)

	row, err := db.Query("SELECT p.id, p.title FROM post p INNER JOIN user u ON p.creator = u.id WHERE p.creator = ?", CookieGetid(r))
	CheckErr(err)
	var posts []structures.Post
	defer row.Close()
	for row.Next() {
		var post structures.Post
		row.Scan(&post.Id, &post.Title)
		posts = append(posts, post)
	}
	return posts
}

func IsConnected(r *http.Request) bool {
	_, err := r.Cookie("session")
	if err != nil {
		return false
	}
	return true
}
