package handlers

import (
	fonctions "forum/controller/fonctions"
	structures "forum/models"
	"net/http"
	"text/template"
)

func AdmHandler(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/admin" {
		NotFound(w, r, http.StatusNotFound)
		return
	}

	files := []string{"view/html/admin.html",
		"view/html/templates/head.html",
		"view/html/templates/footer.html",
		"view/html/templates/header/navbar.html",
		"view/html/templates/header/navbarlogin.html",
		"view/html/templates/header/navbarco.html",
		"view/html/templates/header/navbarsignup.html"}
	t := template.Must(template.ParseFiles(files...))
	_, err := r.Cookie("session")
	if err != nil {
		http.Redirect(w, r, "/login", 302)
	}
	uid := structures.Info{Username: fonctions.CookieGetUsername(r)}
	t.Execute(w, uid)
}
