CREATE TABLE User (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  UUID TEXT NOT NULL,
  email TEXT NOT NULL,
  password TEXT NOT NULL,
  pseudo TEXT NOT NULL,
  first_name TEXT NOT NULL,
  last_name TEXT NOT NULL,
);

CREATE TABLE Post (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  creator INTEGER NOT NULL,
  title TEXT NOT NULL,
  content TEXT NOT NULL,
  favorite INTEGER NOT NULL DEFAULT 0,
  unfavorite INTEGER NOT NULL DEFAULT 0,
  FOREIGN KEY(creator) REFERENCES User(id)
);

INSERT INTO Comment (
  creator,
  post_id,
  content
) VALUES (
  1,
  3
  "Coucou les copains"
), (
  1,
  3,
  "Un autre comment"
);
