package structures

type Post struct {
	Id        int
	Creator   string
	CreatorId int
	Title     string
	Content   string
	Like      int
	Dislike   int
}

type Comment struct {
	Id      int
	Creator string
	Content string
	Like    int
	Dislike int
	PostId  int
}

type User struct {
	Username  string
	Email     string
	FirstName string
	LastName  string
}

type UserPage struct {
	Username string
	User     User
	Posts    []Post
}

type Info struct {
	Username string
	PostsTab []Post
}

type InfoPost struct {
	Username string
	Post     Post
	Comments []Comment
}
