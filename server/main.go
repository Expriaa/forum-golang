package main

import (
	"database/sql"
	"fmt"
	handlers "forum/controller"
	"log"
	"net/http"

	_ "github.com/mattn/go-sqlite3"
)

func main() {

	// Donner accès à notre dossier assets (CSS & Images)
	fs := http.FileServer(http.Dir("./view/assets/"))
	http.Handle("/assets/", http.StripPrefix("/assets/", fs))

	// Définir les fonctions et les routes
	http.HandleFunc("/", handlers.MainHandler)
	http.HandleFunc("/register", handlers.RegHandler)
	http.HandleFunc("/login", handlers.LogHandler)
	http.HandleFunc("/admin", handlers.AdmHandler)
	http.HandleFunc("/info", handlers.InfoHandler)
	http.HandleFunc("/profil", handlers.ProfilsHandler)
	http.HandleFunc("/user", handlers.UserHandler)
	http.HandleFunc("/post", handlers.PostHandler)
	http.HandleFunc("/posts", handlers.PostsHandler)
	http.HandleFunc("/logout", handlers.LogoutHandler)
	http.HandleFunc("/show", handlers.PostIdHandler)

	fmt.Println("Server started at http://localhost:80")

	http.ListenAndServe("localhost:80", nil)
	log.Fatal(http.ListenAndServe(":80", nil))

	//sqlite3 database connection

	db, err := sql.Open("sqlite3", "./database/database.db")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

}
